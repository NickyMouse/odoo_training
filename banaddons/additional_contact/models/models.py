# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class additional_contact(models.Model):
#     _name = 'additional_contact.additional_contact'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Additional_contact(models.Model):
    _name = 'contact.additional'

    # name = fields.Char(string="Nama", required=True)
    contact_name = fields.Many2one('contact.user', string = 'Contact User', ondelete="cascade", required=True)
    contact_type = fields.Many2one('contact.type', string = 'Contact Type', ondelete="cascade", required=True)
    value = fields.Char(string="Value", required=True)
    description = fields.Text()
