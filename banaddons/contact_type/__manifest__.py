# -*- coding: utf-8 -*-
{
    'name': "BAN Contact Type",

    'summary': """
        Contact type like (email, phone, fax, etc)
    """,

    'description': """
        Contact type like (email, phone, fax, etc)
        the part of contact too
    """,

    'author': "Bayu Ady Nugraha",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Just Training to create modules',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base',],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}