# -*- coding: utf-8 -*-
{
'name': "BAN Contact",

    'summary': """
        First Modules that I made
        """,

    'description': """
        This module is about contact person
    """,

    'author': "Bayu Ady Nugraha",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Just Training to create modules',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        # 'views/contact_type/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}