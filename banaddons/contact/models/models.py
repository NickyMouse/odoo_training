# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class contact(models.Model):
#     _name = 'contact.contact'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class contact(models.Model):
    _name = 'contact.user'

    name = fields.Char(string="Nama", required=True)
    nomor_telpon = fields.Char(string='Nomor Telpon')
    description = fields.Text()

