# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class wizardtraining(models.Model):
#     _name = 'wizardtraining.wizardtraining'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Wizard(models.TransientModel):
    _name = 'training.wizard'

    def _default_sesi(self):
        return self.env['training.sesi'].browse(self._context.get('active_id'))

    session_id = fields.Many2one('training.sesi', string="Sesi", required=True, default=_default_sesi)
    attendee_ids = fields.Many2many('res.partner', string="Peserta")

    @api.multi
    def tambah_peserta(self):
        self.session_id.attendee_ids |= self.attendee_ids
        return {}


class Wizardv2(models.TransientModel):
    _name = 'training.wizardv2'

    def _default_sesi(self):
        return self.env['training.sesi'].browse(self._context.get('active_id'))

    def _default_sesis(self):
        return self.env['training.sesi'].browse(self._context.get('active_ids'))

    session_id = fields.Many2one('training.sesi', string="Sesi", default=_default_sesi)
    attendee_ids = fields.Many2many('res.partner', string="Peserta")

    session_ids = fields.Many2many('training.sesi', string="Sesi" , default=_default_sesis)

    @api.multi
    def tambah_peserta(self):
        self.session_id.attendee_ids |= self.attendee_ids
        return {}

    @api.multi
    def tambah_banyak_peserta(self):
        for sesi in self.session_ids:
            sesi.attendee_ids |= self.attendee_ids
        return {}