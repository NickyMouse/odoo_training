# -*- coding: utf-8 -*-
{
    'name': "BAN customers TA",

    'summary': """
        Tugas Akhir
    """,

    'description': """
        Tugas akhir membuat customers
        inherit dari rest partner
        dengan mengurangi dan atau menambah beberapa field
    """,

    'author': "Bayu Ady Nugraha",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Tugas Akhir',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/sales/customer.xml',
        'views/sales/product.xml',
        'views/sales/travel_package.xml',
        'views/sales/sales_order.xml',
        'views/master/bom.xml',
        'views/config/product/uom.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}