# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Test_customers(models.Model):
    _inherit = 'res.partner'

    no_ktp = fields.Char()
    gender = fields.Selection([('M','Man'),('F','Woman')])
    father_name = fields.Char()
    mother_name = fields.Char()
    job = fields.Char()
    cust_type = fields.Char(default="TA")

    place_of_birth = fields.Char()
    date_of_birth= fields.Date()
    blood_type = fields.Selection([('A','A'),('B','B'),('AB','AB'),('O','O')])
    marital_status = fields.Selection([('single','Single'),('married','Married'),('divorce','Divorce')])
    education = fields.Selection([('sd','SD'),('smp','SMP'),('sma','SMA'),('diploma','Diploma'),('s1','S1'),('s2','S2'),('s3','S3')])

    wotp_message = fields.Selection([('Null','No Message')],default='Null')


    # currency_id = fields.Char()
    # property_account_receivable_id = fields.Char()
    # property_payment_term_id = fields.Char()
    # trust = fields.Char()
    # property_account_position_id = fields.Char()
    # debit = fields.Char()
    # property_supplier_payment_term_id = fields.Char()
    # property_account_payable_id = fields.Char()
    # credit = fields.Char()

    # session_ids = fields.Char()
    # instructor = fields.Char()
