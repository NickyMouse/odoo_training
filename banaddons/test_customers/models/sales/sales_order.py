# -*- coding: utf-8 -*-

from odoo import models, fields, api

class travel_document(models.Model):
    _name = 'travel.documents'

    document_travel_package_id = fields.Many2one('sale.order', string='Document')

    document_name = fields.Char()
    document_photo = fields.Binary()


class travel_somanifest(models.Model):
    _name = 'travel.so.manifest'

    so_manifest_travel_package_id = fields.Many2one('sale.order', string='Document')

    so_manifest_jamaah_id = fields.Many2one('res.partner', domain=[('is_company', '=', 'f')])
    so_manifest_ktp_no = fields.Char(readonly=True)
    so_manifest_place_of_birth = fields.Char(readonly=True)
    so_manifest_room_type = fields.Selection([
        ('single', 'Single'),
        ('double', 'Double'),
        ('quad', 'Quad'),
    ])
    so_manifest_title = fields.Char()
    so_manifest_date_of_birth = fields.Date()
    so_manifest_age = fields.Integer()
    so_manifest_mahram_id = fields.Many2one('res.partner', domain=[('is_company', '=', 'f')])

    so_manifest_passport_no = fields.Char()
    document_photo = fields.Binary()


class sales_order(models.Model):
    _inherit = 'sale.order'

    testnow = fields.Char()
    travel_document_line = fields.One2many('travel.documents', 'document_travel_package_id', string='Document Lines')
    travel_so_manifest_line = fields.One2many('travel.so.manifest', 'so_manifest_travel_package_id', string='Manifest')