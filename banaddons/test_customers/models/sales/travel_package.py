# -*- coding: utf-8 -*-

from odoo import models, fields, api

class hotel(models.Model):
    _name = 'travel.hotel'

    hotel_travel_package_id = fields.Many2one('sales.travel.package', string='Hotel')
    hotel_id = fields.Many2one('res.partner', string='Hotel')
    hotel_start_date = fields.Date()
    hotel_end_date = fields.Date()
    hotel_city = fields.Char('City', related='hotel_id.city')

class airline(models.Model):
    _name = 'travel.airline'

    airline_travel_package_id = fields.Many2one('sales.travel.package', string='Hotel')
    airline_id = fields.Many2one('res.partner', string='Airline')
    airline_departure_date = fields.Date()
    airline_departure_city = fields.Char()
    airline_arrival_city = fields.Char()

class schadule(models.Model):
    _name = 'travel.schadule'

    schadule_travel_package = fields.Many2one('sales.travel.package')
    schadule_name = fields.Char()
    schadule_date = fields.Date()

class manifest(models.Model):
    _name = 'travel.manifest'

    manifest_travel_package = fields.Many2one('sales.travel.package')
    manifest_title = fields.Char()
    manifest_gender = fields.Char()
    manifest_passport_name = fields.Char()
    manifest_passport_no = fields.Char()
    manifest_ktp_no = fields.Char()
    manifest_date_of_birth = fields.Date()
    manifest_place_of_birth = fields.Char()
    manifest_date_issued = fields.Date()
    manifest_date_of_expiry = fields.Date()
    manifest_imigrasi = fields.Char()
    manifest_age = fields.Char()
    manifest_room_type = fields.Char()
    manifest_mahram = fields.Char()
    manifest_agent = fields.Char()

class commission(models.Model):
    _name = 'travel.commission'

    commissions_travel_package = fields.Many2one('sales.travel.package')
    commissions_vendor = fields.Char()
    commissions_bill_date = fields.Date()
    commissions_number = fields.Char()
    commissions_vendor_reference = fields.Char()
    commissions_due_date = fields.Date()
    commissions_source_document = fields.Char()
    commissions_total = fields.Char()
    commissions_to_pay = fields.Char()
    commissions_status = fields.Char()

class product(models.Model):
    _name = 'travel.hpp'

    hpp_travel_package_id = fields.Many2one('sales.travel.package', string='HPP')
    product_id = fields.Many2one('product.product')
    product_qty = fields.Float()
    product_uom_id = fields.Many2one('product.uom')
    list_price = fields.Float()
    hpp_subtotal = fields.Float(compute='_subtotal_price')

    @api.depends('product_qty','list_price')
    def _subtotal_price(self):
        for r in self:
            r.hpp_subtotal = r.product_qty*r.list_price

class travel_packages(models.Model):
    _name = 'sales.travel.package'
    _rec_name = 'travel_reference'

    _defaults = {
        'travel_reference': '/',
    }

    # namaste = fields.Char()
    travel_reference = fields.Char(readonly=True)
    travel_departure_date = fields.Date()
    travel_return_date = fields.Date()

    travel_sale_id = fields.Many2one('product.product', domain=[('product_tmpl_id.type', '=', 'service')])
    travel_package_id = fields.Many2one('mrp.bom', domain=[('product_tmpl_id.uom_id.name', '=', 'Paket')])

    travel_quota = fields.Integer()
    travel_quota_progress = fields.Integer(readonly=True)
    travel_remaining_seats = fields.Integer(readonly=True, compute='_change_quota')

    travel_amount = fields.Integer()
    travel_product = fields.Many2one('product.product', domain=[('product_tmpl_id.type', '=', 'product')])

    travel_hotel_line = fields.One2many('travel.hotel', 'hotel_travel_package_id', string='Hotel Lines')
    travel_airline_line = fields.One2many('travel.airline', 'airline_travel_package_id', string='Airline Lines')
    travel_schadule_line = fields.One2many('travel.schadule', 'schadule_travel_package', string='Schadule Lines')
    travel_manifest_line = fields.One2many('travel.manifest', 'manifest_travel_package', string='Manifest Lines')
    travel_hpp_line = fields.One2many('travel.hpp', 'hpp_travel_package_id', string='HPP Lines')
    travel_commissions_line = fields.One2many('travel.commission', 'commissions_travel_package', string='Commission Lines')

    travel_note = fields.Char()

    @api.depends('travel_quota')
    def _change_quota(self):
        for r in self:
            r.travel_remaining_seats = self.travel_quota

    @api.onchange('travel_package_id')
    def _onchange_package(self):
        if self.travel_package_id.id != False :
            changevalue = self.travel_package_id.id
            productmod = self.env['mrp.bom.line']
            onchangeproduct = []
            productlist = []
            onchangeproduct = productmod.search([('bom_id', '=', changevalue)])
            # print(onchangeproduct[0])
            for val in onchangeproduct :
                # print(val)
                # dataproduct = productmod.browse(val)
                productlist.append(
                                    {
                                        'product_id' : val.product_id,
                                        'product_qty' : val.product_qty,
                                        'product_uom_id' : val.product_uom_id,
                                        'list_price' : val.product_id.product_tmpl_id.list_price,
                                        'hpp_subtotal': (val.product_id.product_tmpl_id.list_price * val.product_qty)
                                    }
                )
            # print(productlist)
            return {
                'value': {'travel_hpp_line' : productlist}
            }

    @api.model
    def create(self, vals):
        seq = self.env['ir.sequence'].next_by_code('sales.travel.package') or '/'
        vals['travel_reference'] = seq
        return super(travel_packages, self).create(vals)



