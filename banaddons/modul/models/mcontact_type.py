# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class contact_list(models.Model):
#     _name = 'contact_list.contact_list'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Contact_type(models.Model):
    _name = 'contact.type'

    name = fields.Char(string="Nama", required=True)
    description = fields.Text()

# class Contact_type(models.Model):
#     _name = 'contact.type'
#
#     name = field.Char(string="Nama")