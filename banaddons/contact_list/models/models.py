# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class contact_list(models.Model):
#     _name = 'contact_list.contact_list'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class contact_list(models.Model):
    _name ='contact.friendlist'
    _rec_name = 'contact_user_id'

    contact_user_id = fields.Many2one('contact.user',string='Contact User',ondelete='cascade',required=True)
    contact_friendlist_line = fields.One2many('contact.user','user_id',string='Friendlist')
    description = fields.Text()

class contact(models.Model):
    _inherit = 'contact.user'
    friendlist_id = fields.Many2one('contact.friendlist',string="Friendlist")
    user_id = fields.Many2one('contact.user', string='User')
