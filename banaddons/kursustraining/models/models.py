# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class kursustraining(models.Model):
#     _name = 'kursustraining.kursustraining'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class Kursus(models.Model):
    _name = 'training.kursus'
    _inherit = 'mail.thread'

    name = fields.Char(string="Judul", required=True, readonly=True, states={'draft': [('readonly', False)]},
                       track_visibility='always')
    description = fields.Text(readonly=True, states={'draft': [('readonly', False)]}, track_visibility='onchange')
    # session_ids = fields.One2many('training.sesi', 'course_id', string="Sesi", readonly=True,
    #                               states={'draft': [('readonly', False)]}, track_visibility='onchange')
    responsible_id = fields.Many2one('res.users', ondelete='set null', string="Penanggung Jawab", index=True,
                                     readonly=True, states={'draft': [('readonly', False)]},
                                     track_visibility='onchange')
    # state = fields.Selection([
    #     ('draft', 'Draft'),
    #     ('open', 'Open'),
    #     ('done', 'Done'),
    # ], string='Status', readonly=True, copy=False, default='draft', track_visibility='onchange')
    #
    # @api.multi
    # def action_confirm(self):
    #     self.write({'state': 'open'})
    #
    # @api.multi
    # def action_cancel(self):
    #     self.write({'state': 'draft'})
    #
    # @api.multi
    # def action_close(self):
    #     self.write({'state': 'done'})