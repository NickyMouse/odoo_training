# -*- coding: utf-8 -*-
{
    'name': "BAN friendlist M2M",

    'summary': """
        Contact list (like friend list)    
    """,

    'description': """
         Contact list like friend list , select the person and then select all friend of this person with many to many
    """,

    'author': "Bayu Ady Nugraha",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Just for training',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','contact','contact_list'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}